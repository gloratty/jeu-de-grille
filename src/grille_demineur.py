import  sys;
from  random  import  random;
import  string


def grille(n, p):
    tab = [[0 for i in range(n)] for j in range(n)]; # remplissage de la grille avec des 0 en ligne et colonne
    for i in range(n): # pour chaque ligne
        for j in range(n): # pour chaque colonne
            if 0 <= random() < p: tab[i][j] = "💣"; # remplissage de la grille avec des bombes en fonction de la probabilité p donnée (si random() est compris entre 0 et p, alors on met une bombe)
    return tab; # retourne la grille


# affichage de la grille par ligne et espace entre chaque élément
def affiche(tab):
    for i in tab:
        for j in i:
            print(j, end="  ");
        print();

affiche(grille(6, 0.5)); # affichage de la grille avec 10 lignes et 10 colonnes et une probabilité de 0.2

