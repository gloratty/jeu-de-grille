import sys;
import random;
import string

# On crée une liste des lettres de l'alphabet
alphabet = list(string.ascii_uppercase);
#print(alphabet);

def lesmots():

    # On ouvre le fichier mfrancais.txt en mode lecture
    fichier = open("../doc/les_mots_français.txt", "r" , encoding='utf8', errors='ignore');

    # On lit toutes les lignes du fichier
    lignes = fichier.readlines();

    # On ferme le fichier
    fichier.close();

    # On crée une liste vide
    liste = [];

    # On parcourt toutes les lignes du fichier
    for ligne in lignes:
        # On ajoute la ligne à la liste
        liste.append(ligne.strip()); # strip() permet de supprimer les espaces en début et fin de chaîne

    # On retourne la liste
    return liste;



def grille(n):
    tab = [[0 for i in range(n)] for j in range(n)]; # remplissage de la grille avec des 0 en ligne et colonne
    for i in range(n): # pour chaque ligne
        for j in range(n): # pour chaque colonne
            tab[i][j] = random.choice(alphabet); # remplissage de la grille avec des lettres de l'alphabet
    return tab; # retourne la grille

# affichage de la grille par ligne et espace entre chaque élément
def affiche(tab):
    for i in tab:
        for j in i:
            print(j, end="  ");
        print();

affiche(grille(20)); # affichage de la grille avec 20 lignes et 20 colonnes

print(lesmots());